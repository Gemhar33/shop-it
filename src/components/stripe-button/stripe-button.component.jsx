import React from "react";
import StripeCheckout from "react-stripe-checkout";

const StripeCheckoutButton = ({ price }) => {
  const priceInCents = price * 100;
  const pubKey = "pk_test_YMbt85ohwA44NeogIkHbcdlQ00Rorup89M";

  const onToken = token => {
    console.log(token);
    alert("Payment Successful");
  };

  return (
    <StripeCheckout
      label="Pay Now"
      name="GRafi Labs Ltd."
      shippingAddress
      billingAddress
      image="https://stripe.com/img/documentation/checkout/marketplace.png"
      description={`Your total is$${price}`}
      amount={priceInCents}
      panelLabel="Pay Now"
      token={onToken}
      stripeKey={pubKey}
    />
  );
};

export default StripeCheckoutButton;
