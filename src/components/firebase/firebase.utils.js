import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/auth";

const config = {
  apiKey: "AIzaSyBBBI0LnwlwqOEJqIFeto1HU_GaRa_2iBs",
  authDomain: "shop-it-db.firebaseapp.com",
  databaseURL: "https://shop-it-db.firebaseio.com",
  projectId: "shop-it-db",
  storageBucket: "shop-it-db.appspot.com",
  messagingSenderId: "928076765747",
  appId: "1:928076765747:web:c7bf3dbd19c96d3893e146",
  measurementId: "G-YT2GFJKX51"
};

export const createUserProfileDocument = async (userAuth, additionalData) => {
  if (!userAuth) return;

  const userRef = firestore.doc(`users/${userAuth.uid}`);
  const snapShot = await userRef.get();

  if (!snapShot.exist) {
    const { displayName, email } = userAuth;
    const created_at = new Date();

    try {
      await userRef.set({
        displayName,
        email,
        created_at,
        ...additionalData
      });
    } catch (error) {
      console.log("error in user creation", error);
    }

    return userRef;
  }
};

firebase.initializeApp(config);

export const auth = firebase.auth();
export const firestore = firebase.firestore();

const provider = new firebase.auth.GoogleAuthProvider();

provider.setCustomParameters({ prompt: "select_account" });
export const signInWithGoogle = () => auth.signInWithPopup(provider);

export default firebase;
