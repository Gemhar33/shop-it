import cartTypes from "./cart.types";

export const toggleCartDropdown = () => ({
  type: cartTypes.TOGGLE_CART_HIDDEN
});

export const addCartItem = payload => ({
  type: cartTypes.ADD_ITEM,
  payload
});

export const removeCartItem = payload => ({
  type: cartTypes.REMOVE_ITEM,
  payload
});

export const clearItemFromCart = payload => ({
  type: cartTypes.CLEAR_ITEM_FROM_CART,
  payload
});
